<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/formulir', function () {
    return view('formulir');
});

Route::post('/kirim', 'Controller@kirimform' );

// Route::get('ID/{id}',function($id){ echo 'isi ID adalah : '.$id; }); 
 

// Route::get('ID/{id}/{nama}',function($id,$nama){ echo 'NRP : '.$id .' nama : ' .$nama ; }); 


Route::get('ID/{id}/{nama}/{alamat?}',function($id,$nama,$alamat='kejawan putih'){ echo 'NRP : '.$id .' nama : ' .$nama .'alamat : ' .$alamat; }); 

Route::get('/formget', function () {
return view('formget');
});


Route::get('/formpost', function () {
return view('formpost');
});

Route::get('/abc', function () {
return view('abc');
});

Route::match(['get', 'post'],'/submitPost', 'Controller@submitForm');

Route::resource('mhs','mhsController');

Route::resource('mhs','mhsControllerAll');
