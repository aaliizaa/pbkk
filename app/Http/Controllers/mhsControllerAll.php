<?php

namespace App\Http\Controllers;

use App\mhs;

use Illuminate\Http\Request;

use App\Http\Requests\mhs\StoreRequest;
use App\Http\Requests\mhs\UpdateRequest;


class mhsControllerAll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      
        // 
        $data['mhs'] = mhs::all();        
        return view('mhs.index',$data);    
    } 
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return view('mhs.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        //
        $m = new mhs();        
        $m->nrp = $request->nrp;        
        $m->nama = $request->nama;      
        $m->alamat = $request->alamat;        
        $m->save();        
        return redirect()->route('mhs.index')->with('alert-success', 'Data Berhasil Disimpan.');    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $m = mhs::findOrFail($id); 
        return view('mhs.edit', compact('m')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        //
        $m = mhs::findOrFail($id);      
        $m->nrp = $request->nrp;        
        $m->nama = $request->nama;        
        $m->alamat = $request->alamat;        
        $m->save();        
        return redirect()->route('mhs.index')->with('alert-success', 'Data Berhasil Diubah.');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $m = mhs::findOrFail($id);       
        $m->delete();
        return redirect()->route('mhs.index')->with('alert-success', 'Data Berhasil Dihapus.');   
    }
}
